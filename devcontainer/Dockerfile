ARG FROM_PREFIX
ARG FROM_SOURCE
ARG FROM_TAG
ARG BUILD_TAG

FROM ${FROM_PREFIX}${FROM_SOURCE}:${FROM_TAG}
# ex: debian:bullseye-slim, alpine

# Good to know: FROM_SOURCE really needed to be repeated here for the re-use further below to work.
ARG BUILD_TAG
ARG FROM_SOURCE

# Copy devcontainer scripts
COPY --from=geekstuffreal/devcontainer:v0.14 /scripts/ /devcontainer/

# Setup devcontainer
RUN /devcontainer/basics.sh

# Setup tools
RUN /devcontainer/tools/direnv.sh
RUN /devcontainer/tools/terraform.sh
RUN /devcontainer/tools/vault.sh

# Setup user
RUN /devcontainer/user/starship.sh

# Install GNU diff in Alpine image
RUN set -e && \
    if command -v apk 2>/dev/null 1>/dev/null; then \
        echo "installing APK dependencies"; \
        apk add --update --no-cache diffutils grep; \
    else \
        true; \
    fi

# Lib ENV vars
ARG DC_LIB=/devcontainer-template
ENV DC_TAG=${BUILD_TAG}
ENV DC_LIB=${DC_LIB}
ENV DC_LIB_DIRENV=${DC_LIB}/direnv
ENV DC_LIB_MAKE=${DC_LIB}/make
ENV DC_LIB_TF=${DC_LIB}/terraform
ENV DC_TEMPLATE=${DC_LIB}/template

# Add BUILD_TAG
ENV BUILD_TAG=${BUILD_TAG}

# Copy devcontainer scripts
COPY ./devcontainer/direnv/ ${DC_LIB_DIRENV}/
COPY ./devcontainer/make/ ${DC_LIB_MAKE}/

# Copy script to make vault-login cmd.exe call to launch web browser work
COPY ./devcontainer/cmd.exe /usr/local/bin/cmd.exe

# Template users should use (could be used later in an update process)
COPY ./user-template/ ${DC_TEMPLATE}/
RUN (grep -qRn '%DC_LIB_VERSION%' ${DC_TEMPLATE} && ( \
      grep -rl '%DC_LIB_VERSION%' ${DC_TEMPLATE} \
      | xargs sed -i "s/%DC_LIB_VERSION%/${BUILD_TAG}/g" \
    ) || true) \
    && (grep -qRn '%DC_SOURCE%' ${DC_TEMPLATE} && ( \
      grep -rl '%DC_SOURCE%' ${DC_TEMPLATE} \
      | xargs sed -i "s/%DC_SOURCE%/${FROM_SOURCE}/g" \
    ) || true)

# Workdir
WORKDIR /home/dev

# Setup ONBUILD to auto apply user gitconfig overrides if any were passed
ONBUILD ARG GIT_EMAIL
ONBUILD ARG GIT_SIGNINGKEY
ONBUILD RUN GIT_EMAIL=${GIT_EMAIL} \
    GIT_SIGNINGKEY=${GIT_SIGNINGKEY} \
        /devcontainer/basics/4-gitconfig.sh
