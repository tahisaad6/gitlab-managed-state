# A Devcontainer template, for Terraform

This projects builds:
- a [devcontainer image](./devcontainer): gives the dev environment
- a [gitlab-ci image](./gitlab-ci-image): helps in pipelines
- end user [template packages](./user-template): with bells and whistles
- holds a [gitlab-ci library](./gitlab-ci-kits): usable from any gitlab instances

Template packages now also includes 3 terraform template to get started with basic and secure ways.

## How to use

- Open the latest [gitlab release](https://gitlab.com/geekstuff.dev/templates/terraform/gitlab-managed-state/-/releases)
- Follow the instructions
- Open folder with VSCode
    - It should suggest you to install the [remote container extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
    - A `Reopen in container` popup should then appear
    - Otherwise F1, search and run `Reopen in Container`

Note: VSCode may get annoying with popups if you already have many extensions. You may have
      dialog boxes all ignored since long ago, and need to do 1 or 2 steps manually to setup and start.
