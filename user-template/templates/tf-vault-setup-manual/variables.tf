variable "gitlab_vault_jwt_project_id" {
  description = "gitlab project ID to give this pipeline access"
  type        = number
}

variable "project_name" {
  description = "Project name will be used as a prefix to everything in Vault"
  type        = string
}
